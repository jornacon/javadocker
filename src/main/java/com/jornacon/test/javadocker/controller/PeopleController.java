package com.jornacon.test.javadocker.controller;

import com.jornacon.test.javadocker.dao.PersonDAO;
import com.jornacon.test.javadocker.dto.PersonDTO;
import com.jornacon.test.javadocker.mappers.PeopleMapper;
import com.jornacon.test.javadocker.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/people")
public class PeopleController {
    private final PeopleService service;
    private final PeopleMapper peopleMapper;

    @Autowired
    public PeopleController(PeopleService service) {
        this.service = service;
        peopleMapper = new PeopleMapper();
    }

    @GetMapping("/")
    public List<PersonDTO> retrieve() {
        List<PersonDAO> people = service.getPeople();
        return peopleMapper.mapToDTO(people);
    }

    @PostMapping("/")
    public void newPerson(@RequestBody PersonDTO personDTO) {
        service.savePerson(peopleMapper.map(personDTO));
    }

    @PostMapping("/group")
    public void newGroupPeople(@RequestBody List<PersonDTO> peopleDTO) {
        service.savePeople(peopleMapper.mapToDAO(peopleDTO));
    }
}
