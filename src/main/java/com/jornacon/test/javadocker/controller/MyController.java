package com.jornacon.test.javadocker.controller;

import com.jornacon.test.javadocker.dto.GreetingDTO;
import com.jornacon.test.javadocker.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class MyController {
    private static final String template = "Hello, %s, ip: %s!";
    private final AtomicLong counter = new AtomicLong();
    private final MyService service;

    @Autowired
    public MyController(MyService service) {
        this.service = service;
    }

    @RequestMapping("/greeting")
    public GreetingDTO greeting(@RequestParam(value="name", defaultValue="World") String name) throws Exception {
        return new GreetingDTO(counter.incrementAndGet(),
            String.format(template, name, service.getServerAddress()));
    }
}
