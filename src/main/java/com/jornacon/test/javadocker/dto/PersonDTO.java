package com.jornacon.test.javadocker.dto;

public class PersonDTO {
    private PersonDTO() {
    }

    public String id;
    public String name;
    public String email;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public static class PersonBuilder {
        public String id;
        public String name;
        public String email;

        public PersonBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public PersonBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public PersonBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public PersonDTO build() {
            PersonDTO personDTO = new PersonDTO();
            personDTO.id = this.id;
            personDTO.name = this.name;
            personDTO.email = this.email;
            return personDTO;
        }
    }
}
