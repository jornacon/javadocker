package com.jornacon.test.javadocker.service;

public interface MyService {
    String getServerAddress() throws Exception;
}
