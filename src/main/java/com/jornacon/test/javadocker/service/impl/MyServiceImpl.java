package com.jornacon.test.javadocker.service.impl;

import com.jornacon.test.javadocker.service.MyService;
import org.springframework.stereotype.Service;

import java.net.InetAddress;

@Service
public class MyServiceImpl implements MyService {
    @Override
    public String getServerAddress() throws Exception {
        final String serverAddress = InetAddress.getLocalHost().getHostAddress();
        return serverAddress;
    }
}
