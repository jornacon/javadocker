package com.jornacon.test.javadocker.service;

import com.jornacon.test.javadocker.dao.PersonDAO;

import java.util.List;

public interface PeopleService {
    List<PersonDAO> getPeople();
    void savePerson(PersonDAO personDAO);
    void savePeople(List<PersonDAO> personDAOS);
}
