package com.jornacon.test.javadocker.service.impl;

import com.jornacon.test.javadocker.dao.PersonDAO;
import com.jornacon.test.javadocker.service.PeopleService;
import com.jornacon.test.javadocker.store.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeopleServiceImpl implements PeopleService {
    @Autowired
    private PersonRepository repository;

    @Override
    public List<PersonDAO> getPeople() {
        return repository.findAll();
    }

    @Override
    public void savePerson(PersonDAO personDAO) {
        repository.save(personDAO);
    }

    @Override
    public void savePeople(List<PersonDAO> people) {
        repository.saveAll(people);
    }
}
