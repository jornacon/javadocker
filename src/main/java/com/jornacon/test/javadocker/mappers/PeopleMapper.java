package com.jornacon.test.javadocker.mappers;

import com.jornacon.test.javadocker.dao.PersonDAO;
import com.jornacon.test.javadocker.dto.PersonDTO;

import java.util.List;
import java.util.stream.Collectors;

public class PeopleMapper {

    public PersonDAO map(PersonDTO personDTO) {
        return new PersonDAO(personDTO.getName(), personDTO.getEmail());
    }

    public List<PersonDAO> mapToDAO(List<PersonDTO> peopleDTO) {
        return peopleDTO.stream().map(personDTO -> new PersonDAO(personDTO.getName(), personDTO.getEmail())).collect(Collectors.toList());
    }

    public PersonDTO map(PersonDAO personDAO) {
        PersonDTO.PersonBuilder builder = new PersonDTO.PersonBuilder();
        builder.withId(personDAO.getId());
        builder.withName(personDAO.getName());
        builder.withEmail(personDAO.getEmail());
        return builder.build();
    }

    public List<PersonDTO> mapToDTO(List<PersonDAO> people) {
        return people.stream().map(this::map).collect(Collectors.toList());
    }

}
