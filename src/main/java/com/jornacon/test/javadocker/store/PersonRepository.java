package com.jornacon.test.javadocker.store;

import com.jornacon.test.javadocker.dao.PersonDAO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PersonRepository extends MongoRepository<PersonDAO, String> {
}
