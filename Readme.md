#Spring / MongoDB / Docker project

####Minimum requirements
- Docker version 18.09.7
- docker-compose version 1.24.0

####Descripion
After running the command in the description you will have a springboot application running, connected to a mongodb filled with 2 documents (just for the example), and a loadbalancer to be able to increase the number of running instances  

####Run the application
Go to the folder where docker-compose.yml file is and run:

- ``docker-compose up``

If you want to run on background run:

- ``docker-compose up -d``

After executing one of those commands, you can increase the number of services running just executing:

- ``docker-compose scale javadocker=3``

####URL
See the IP from docker instance

``localhost/greeting``

See database documents

``localhost/persons``

####Enjoy!